//Given an unsorted integer array nums. Return the smallest positive integer that is not present in nums.
//        You must implement an algorithm that runs in O(n) time and uses O(1) auxiliary space.

import java.util.*;

public class task4 {
    public static void main(String[] args) {

        int[] nums = {-1,1,7,8,9,11,12};

        HashMap<Integer, Boolean> map = new HashMap<Integer, Boolean>();


        for (int num : nums) {
            if (num > 0) {
                map.put(num, true);
            }
        }


        int smallestMissing = 1;
        while (map.get(smallestMissing) != null) {
            smallestMissing++;
        }

        System.out.println("Smallest positive integer " + smallestMissing);
    }
}
