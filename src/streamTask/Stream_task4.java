import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//
//Task 4
//        Stream istifade etmekle olchusu (size) 50 olan elementleri random generasiya olunmush List<Double> listi yaradin


public class Stream_task4 {
    public static void main(String[] args) {


        List<Double> doubles=  Stream.generate(()-> new Random().nextDouble())
                .limit(50)
                .collect(Collectors.toList());


        doubles.forEach(System.out::println);;
    }


}

