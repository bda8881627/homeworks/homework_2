package StreamTask4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Data {
    public static void main(String[] args) {
        List<Person> persons =new ArrayList<>();
        persons.add(new Person("Ali", 255));
        persons.add(new Person("Babek", 30));
        persons.add(new Person("Rustem", 35));
        persons.add(new Person("Rashad", 45));
        persons.add(new Person("Alikram", 15));
        persons.add(new Person("Nail", 60));
        persons.add(new Person("Camil", 13));
        persons.add(new Person("Heyran", 29));
        persons.add(new Person("Aldgi", 275));
        persons.add(new Person("Babdgek", 37));
        persons.add(new Person("Rustdfgem", 31));
        persons.add(new Person("Rasdghad", 48));
        persons.add(new Person("Alidgkram", 26));
        persons.add(new Person("Naidgl", 70));
        persons.add(new Person("Camdgdil", 21));
        persons.add(new Person("Heyrdgan", 22));
        persons.add(new Person("Alig", 25));
        persons.add(new Person("Babedgk", 33));
        persons.add(new Person("Rustemdg", 35));
        persons.add(new Person("Rashaddgdf", 45));
        persons.add(new Person("Alikramdgd", 15));
        persons.add(new Person("Naildgdg", 80));
        persons.add(new Person("Camildgd", 19));
        persons.add(new Person("Heyrangd", 22));
        persons.add(new Person("ewter", 52));

        List<String> sampleNames =new ArrayList<>();

        sampleNames=persons.stream()
                .filter(p-> p.getAge() >= 18 && p.getAge() <= 80 )
                .limit(5)
                .sorted()
                .map(p -> p.getName() + " - " + p.getAge())
                .collect(Collectors.toList());

        sampleNames.forEach(System.out::println);
    }
}
