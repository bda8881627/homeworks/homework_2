import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

//Task 3
//        List<String> sentences = List.of(
//        "Java streams are powerful",
//        "Streams allow functional programming in Java",
//        "Functional programming is powerful"
//        );
//
//        sentences listindeki her elementin daxilindeki sozleri yeni bir List<String> listine elave edin, response ashaghidaki kimi olsun:
//
//        [Java, streams, are, powerful, Streams, allow, functional, programming, in, Java, Functional, programming, is, powerful]



public class Stream_task3 {
    public static void main(String[] args) {

        List<String> sentences = List.of(
                "Java streams are powerful",
                "Streams allow functional programming in Java",
                "Functional programming is powerful"
        );


        List<String> wordList=  sentences.stream()
                .flatMap(sentence -> Arrays.stream(sentence.split(" ")))
                .collect(Collectors.toList());


        System.out.println("Word list");
        System.out.println(wordList);

    }
}
