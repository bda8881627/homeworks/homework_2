import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//Task 2
//        List<String> listinde Long type-a cast ola bilen elementleri Stream istifade etmekle
//        yeni bir List<Long> listi sheklinde gaytaran metod yazmag

public class Stream_task2 {
    public static void main(String[] args) {

        List<String> list1=new ArrayList<>();
        list1.add("eewfe");
        list1.add("12412434");
        list1.add("wwerff");
        list1.add("2134");



        List<Long> list2=new ArrayList<>();


        list2=  list1.stream()
                .filter(s -> {
                    try {
                        Long.parseLong(s);
                        return true;
                    }
                    catch (NumberFormatException e){
                        return false;
                    }
                })
                .map(Long::parseLong)
                .collect(Collectors.toList());


        System.out.println("Number list");
        list2.forEach(System.out::println);


    }
}
