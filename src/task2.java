//Task 2:
//     In array find missing number in the range: [0, n]

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class task2 {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);


        System.out.println("Enter size");
        int endDiapazon=s.nextInt();


        List<Integer> arrayList=new ArrayList<>();
        System.out.println("Enter element array");

        for (int i = 0; i < endDiapazon; i++) {
            int value=s.nextInt();
            arrayList.add(value);
        }

        System.out.println("Missing number:");
        for (int i = 0; i <= endDiapazon; i++) {
            boolean found = false;
            for (int num : arrayList) {
                if (num == i) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                System.out.println(i);
            }
        }
    }
}
